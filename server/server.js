import path from 'path';
import { Server } from 'http';
import Express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'mobx-react';

//

import stores from '../sources/app/stores';

import App from '../sources/app';

//

const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'production';

const app = new Express();
const server = new Server(app);

app.use(Express.static(path.join(__dirname, '/../web'), {
    index: false
}));

app.get('*', (req, res) => {
    const context = {};
    const markup = renderToString(
        <Provider {...stores}>
            <StaticRouter location={req.url} context={context}>
                <App/>
            </StaticRouter>
        </Provider>
    );

    let status = 200;

    if (context.url) {
        return res.redirect(302, context.url);
    }

    if (context.is404) {
        status = 404;
    }

    return res.status(status).send(`
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

                <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
                <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">
                <link rel="stylesheet" href="/assets/jquery-ui/jquery-ui.css">

                <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

                <script src="/assets/jquery/jquery.min.js"></script>
                <script src="/assets/jquery-ui/jquery-ui.min.js"></script>

                <script src="/assets/bootstrap/bootstrap.min.js"></script>

                <title>React Skeleton</title>
            </head>
            <body>
                <div id="root">${markup}</div>

                <script src="/bundle/polyfill.js"></script>
                <script src="/bundle/bundle.js"></script>
            </body>
        </html>
    `);
});

server.listen(port, (err) => {
    if (err) {
        return console.error(err);
    }

    console.info(`Server running on http://localhost:${port}`);
});
