const path = require('path');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');

const publicDir = path.join(__dirname, '/web');
const bundleDir = path.join(publicDir, '/bundle');

module.exports = {
    entry: {
        polyfill: 'babel-polyfill',
        bundle: path.join(__dirname, '/sources/index.js')
    },
    output: {
        path: bundleDir,
        publicPath: '/bundle/',
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                enforce: 'pre',
                exclude: /node_modules/,
                loader: 'eslint-loader'
            },
            {
                test: /\.html$/,
                exclude: /node_modules/,
                loader: 'html-loader',
                options: {
                    interpolate: false,
                    minimize: true
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                loaders: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader']
            },
            {
                test: /\.json$/,
                exclude: /node_modules/,
                loader: 'json-loader'
            },
            {
                test: /\.(jpe?g|png|bmp|gif|svg|woff|woff2|eot|ttf|ogg)$/,
                exclude: /node_modules/,
                loader: 'url-loader',
                options: {
                    name: 'resources/[hash:12].[ext]',
                    limit: 1000
                }
            }
        ]
    },
    plugins: [
        new WebpackCleanupPlugin()
    ],
    resolve: {
        modules: [
            path.join(__dirname, '/sources'),
            path.join(__dirname, '/node_modules')
        ]
    },

    devServer: {
        host: '0.0.0.0',
        port: 4200,
        // proxy: {
        //     '/api': {
        //         target: 'http://127.0.0.1:3000/api',
        //         pathRewrite: {
        //             '^/api': ''
        //         }
        //     },
        //     '/socket': {
        //         target: 'http://127.0.0.1:3000',
        //         ws: true
        //     }
        // },
        contentBase: publicDir,
        publicPath: '/bundle/',
        historyApiFallback: {
            index: 'index.html'
        },
        stats: 'minimal',
        compress: true,
        overlay: true,
        // https: false,
        // open: true
    },

    devtool: 'eval inline-source-map'
};
