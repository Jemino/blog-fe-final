import { observable, computed, action, runInAction } from 'mobx';

export default class AlertStore {
    @observable isOpen = false;
    @observable alertType = 'success';
    @observable alertMessage = 'default_text';

    @action
    showSuccess(alertMessage, delay = 2000) {
        this.alertType = 'success';
        this.showAlert(alertMessage, delay);
    }

    @action
    showWarning(alertMessage, delay = 2000) {
        this.alertType = 'warning';
        this.showAlert(alertMessage, delay);
    }

    @action
    showDanger(alertMessage, delay = 2000) {
        this.alertType = 'danger';
        this.showAlert(alertMessage, delay);
    }

    @action
    showAlert(alertMessage, delay = 2000) {
        this.alertMessage = alertMessage;
        this.isOpen = true;

        if (delay !== -1) {
            setTimeout(() => {
                this.hideAlert();
            }, delay);
        }
    }

    @action
    hideAlert() {
        this.isOpen = false;
    }
}
