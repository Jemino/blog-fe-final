import AuthStore from './AuthStore';
import ArticlesStore from './ArticlesStore';
import AlertStore from './AlertStore';

export default {
    authStore: new AuthStore(),
    articlesStore: new ArticlesStore(),
    alertStore: new AlertStore()
};
