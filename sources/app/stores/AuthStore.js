import { observable, computed, action, runInAction } from 'mobx';

import { login, logout } from '../actions/auth';

export default class AuthStore {
    @observable user = null;
    @observable isLoading = false;

    constructor() {
        this.user = JSON.parse(localStorage.getItem('user'));
    }

    @computed
    get isAuthenticated() {
        return !!this.user;
    }

    @computed
    get token() {
        return this.isAuthenticated ? this.user.token : '';
    }

    @action
    async login(email, password) {
        this.isLoading = true;

        const { data } = await login(email, password);

        if (data.status) {
            localStorage.setItem('user', JSON.stringify(data.user));

            runInAction(() => {
                this.isLoading = false;
                this.user = data.user;
            });
        } else {
            localStorage.removeItem('user');

            runInAction(() => {
                this.isLoading = false;
            });
        }
        return data;
    }

    @action
    async logout() {
        this.isLoading = true;
        //this.user = null;
        //localStorage.removeItem('user');

        const { data } = await logout();

        runInAction(() => {
            if (data.status) {
                this.user = null;
                localStorage.removeItem('user');
            }

            this.isLoading = false;
        });

        console.log(data);
        return data;

    }
}
