import { observable, computed, action } from 'mobx';
import moment from 'moment';

export default class ArticlesStore {
    @observable articles = [
        {
            _id: 1,
            title: 'test_title1',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis odio pellentesque, tempor sem sed, rutrum dui. Ut non libero sit amet lacus rhoncus porta. Sed et pretium lectus, quis iaculis arcu. Etiam id enim convallis, tempor velit vel, condimentum arcu. Etiam non feugiat ligula. Donec finibus, odio at laoreet',
            image: 'http://via.placeholder.com/1350x650',
            tags: ['#tag1', '#tag2'],
            author: { _id: 1, username: 'testtest1' },
            ratingCount: 1,
            createdAt: Math.floor(Date.now() - (Math.random() * 3600 * 60 * 60))
        },
        {
            _id: 2,
            title: 'test_title2',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis odio pellentesque, tempor sem sed, rutrum dui. Ut non libero sit amet lacus rhoncus porta. Sed et pretium lectus, quis iaculis arcu. Etiam id enim convallis, tempor velit vel, condimentum arcu. Etiam non feugiat ligula. Donec finibus, odio at laoreet',
            image: 'http://via.placeholder.com/1350x650',
            tags: ['#tag1', '#tag3', '#tag4'],
            author: { _id: 2, username: 'testtest2' },
            ratingCount: 5,
            createdAt: Math.floor(Date.now() - (Math.random() * 3600 * 60 * 60))
        },
        {
            _id: 3,
            title: 'test_title3',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis odio pellentesque, tempor sem sed, rutrum dui. Ut non libero sit amet lacus rhoncus porta. Sed et pretium lectus, quis iaculis arcu. Etiam id enim convallis, tempor velit vel, condimentum arcu. Etiam non feugiat ligula. Donec finibus, odio at laoreet',
            image: 'http://via.placeholder.com/1350x650',
            tags: ['#tag5', '#tag3', '#tag2'],
            author: { _id: 2, username: 'testtest2' },
            ratingCount: 10,
            createdAt: Math.floor(Date.now() - (Math.random() * 3600 * 60 * 60))
        },
        {
            _id: 4,
            title: 'test_title4',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis odio pellentesque, tempor sem sed, rutrum dui. Ut non libero sit amet lacus rhoncus porta. Sed et pretium lectus, quis iaculis arcu. Etiam id enim convallis, tempor velit vel, condimentum arcu. Etiam non feugiat ligula. Donec finibus, odio at laoreet',
            image: 'http://via.placeholder.com/1350x650',
            tags: ['#tag1', '#tag3', '#tag4'],
            author: { _id: 1, username: 'testtest1' },
            ratingCount: -3,
            createdAt: Math.floor(Date.now() - (Math.random() * 3600 * 60 * 60))
        }
    ];

    @observable articlesDump = null;
    @observable searchDump = null;
    @observable chosenTag = null;
    @observable chosenUser = null;
    @observable chosenFilter = 'rating';

    @action
    addArticle(article) {
        this.articles.push(article);
    }

    @action
    deleteArticle(id) {
        this.articles = this.articles.filter(article => article._id !== id);
    }

    @action
    updateRatingCount(article, ratingChange) {
        article.ratingCount = article.ratingCount + ratingChange;
    }

    @action
    updateArticle(id, newData) {
        const article = this.articles.find(article => article._id === id);

        if (!article) {
            return;
        }

        article.tags = newData.tags.split(' ');
        article.title = newData.title;
        article.description = newData.description;
    }

    @action
    filterBy() {
        let tag = this.chosenTag;
        let user = this.chosenUser;
        this.articlesDump = this.articlesDump || this.articles;

        //console.log(tag, user);

        const articlesFilteredByTag = this.articlesDump.filter((article) => {
            if (tag) {
                if (article.tags.indexOf(tag) !== -1) {
                    return true;
                }
            } else {
                return true;
            }
        });

        this.articles = articlesFilteredByTag.filter((article) => {
            if (user) {
                if (user === article.author._id) {
                    return true;
                }
            } else {
                return true;
            }
        });
    }

    @action
    updateSort(key) {

        switch (key) {
            case 'ts':
                this.articles = this.articles.sort((a, b) => {
                    return b.createdAt - a.createdAt;
                });
                break;
            default:
                this.articles = this.articles.sort((a, b) => {
                    return b.ratingCount - a.ratingCount;
                });
        }

    }

    @action
    searchArticles(key) {
        // Authors
        this.searchDump = this.articles;
        this.articles = this.searchDump.filter((article) => {
            if (key) {
                if (key === article.author.username) {
                    return true;
                }
            } else {
                return true;
            }
        });
    }
}
