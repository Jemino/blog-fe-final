import axios from 'axios';

import stores from '../stores';

export function login(email, password) {
    return axios.post(`http://127.0.0.1:3000/security/login`, {
        email,
        password
    });
}

export function logout() {
    return axios.get(`http://127.0.0.1:3000/security/logout`, {
        headers: {
            Authorization: stores.authStore.token
        }
    });
}
