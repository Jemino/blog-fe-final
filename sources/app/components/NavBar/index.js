import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';

@inject('articlesStore', 'alertStore')
@observer
export default class NavBar extends Component {
    @observable activeSort = 'rating';
    @observable search = '';

    @action
    handleSortChange(sortType) {
        const { articlesStore } = this.props;

        this.activeSort = sortType;
        articlesStore.updateSort(sortType);
    }

    @action
    handleSearchChange(event) {
        this.search = event.target.value;

        if (!this.search) {
            console.log('clearFilter');
        }
    }

    @action
    handleSearchSubmit(event) {
        const { articlesStore } = this.props;

        event.preventDefault();

        articlesStore.searchArticles(this.search);
    }

    render() {
        return (
            <div className="navbar-component">
                <nav className="navbar navbar-default navbar-static-top">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span className="icon-bar"/> <span className="icon-bar"/> <span className="icon-bar"/>
                            </button>
                            <a className="navbar-brand" href="#">BloG</a>
                        </div>

                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav">
                                <li className={this.activeSort === 'rating' && 'active'}>
                                    <a href="javascript:void(0);" onClick={() => this.handleSortChange('rating')}>Hot</a>
                                </li>
                                <li className={this.activeSort === 'ts' && 'active'}>
                                    <a href="javascript:void(0);" onClick={() => this.handleSortChange('ts')}>Recent</a>
                                </li>
                            </ul>
                            <form className="navbar-form navbar-right" role="search" onSubmit={(e) => this.handleSearchSubmit(e)}>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Search" value={this.search} onChange={(e) => this.handleSearchChange(e)}/>
                                </div>
                            </form>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}
