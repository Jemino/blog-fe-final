import './styles/index.scss';

import React, { Component } from 'react';

export default class Loader extends Component {
    constructor(props) {
        super(props);

        this.state = {

        };
    }

    render() {
        // const {  } = this.state;

        return (
            <div className="loader-component">
                <div className="cssload-container">
                    <ul className="cssload-flex-container">
                        <li>
                            <span className="cssload-loading cssload-one"/>
                            <span className="cssload-loading cssload-two"/>
                            <span className="cssload-loading-center"/>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}
