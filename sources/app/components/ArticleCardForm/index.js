import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('articlesStore')
@observer
export default class ArticleCardForm extends Component {
    constructor(props) {
        super(props);

        this.state = {

        };
    }

    render() {
        const { articlesStore } = this.props;

        return (
            <div className="article-card-form">
                <div className="thumbnail">

                    <div className="btn-group article-actions" role="group">

                        <button type="button" className="btn btn-success" onClick={() => articlesStore.addArticle({
                            _id: Math.random(),
                            title: 'fdhjfgjfghj',
                            description: 'dghdfghdfghdfgh',
                            image: 'http://via.placeholder.com/1350x350',
                            tags: ['#asda', '#fghfgh'],
                            author: { _id: 1, username: 'testtest' },
                            ratingCount: 100
                        })}>
                            <span className="glyphicon glyphicon-floppy-saved"/>
                        </button>

                    </div>

                    <img className="img-responsive" src="http://via.placeholder.com/1350x350" alt="..."/>

                    <div className="caption">

                        <input type="text" className="form-control tags-edit" placeholder="Tags..."/>

                        <input type="text" className="form-control title-edit" placeholder="Title..."/>

                        <textarea className="form-control description-edit" placeholder="Description..."/>

                    </div>
                </div>
            </div>
        );
    }
}
