import React, { Component } from 'react';
import { observable, action, runInAction } from 'mobx';
import { observer, inject } from 'mobx-react';
import Loader from '../Loader';
import { isEmail } from 'validator';

function validate(form) {
    for (let key in form) {
        const field = form[key];

        if (typeof field !== 'object') {
            continue;
        }

        field.value = field.value.trim();
        field.message = '';

        if (field.require) {
            if (field.value === '') {
                field.message = 'This filed is required';
                return false;
            }
        }

        if (field.minLength) {
            if (field.value.length < field.minLength) {
                field.message = `Minimum length is ${field.minLength} symbols`;
                return false;
            }
        }

        if (field.maxLength) {
            if (field.value.length > field.maxLength) {
                field.message = `Maximum length is > ${field.maxLength} symbols`;
                return false;
            }
        }

        if (field.validate) {
            for (let i = 0; i < field.validate.length; i++) {
                if (field.validate[i].validator(field.value) === false) {
                    field.message = field.validate[i].message;
                    return false;
                }
            }
        }
    }

    return true;
}

@inject('authStore', 'alertStore')
@observer
export default class AuthForm extends Component {
    @observable form = {
        email: {
            require: true,
            minLength: 5,
            maxLength: 20,
            validate: [
                {
                    validator: (value) => {
                        return isEmail(value);
                    },
                    message: 'Please enter valid email'
                }
            ],
            value: ''
        },
        password: {
            require: true,
            minLength: 6,
            maxLength: 16,
            value: ''
        },
        render: Math.random()
    };

    @action
    handleChange(event, key) {
        this.form[key].value = event.target.value;

        validate(this.form);
    }

    @action
    async handleLogin(event) {
        event.preventDefault();

        if (validate(this.form) === false) {
            this.form.render = Math.random();
            return;
        }

        const { authStore, alertStore } = this.props;

        const response = await authStore.login(this.form.email.value, this.form.password.value);

        if (response.status) {
            alertStore.showSuccess('Successfully logged-in');
        } else {
            alertStore.showWarning('Can\'t log-in: ' + response.message);
        }
    }

    render() {
        const { authStore } = this.props;

        return (
            <div className="auth-form-component">
                {authStore.isLoading &&
                <Loader/>
                }

                <div className="panel panel-primary">
                    <form className="panel-body" onSubmit={(event) => this.handleLogin(event)}>
                        <div className={`form-group ${this.form.email.message && 'has-error'}`}>
                            <label className="control-label">Email</label>
                            <input className="form-control" type="text" placeholder="example@example.com" value={this.form.email.value} onChange={(event) => this.handleChange(event, 'email')}/>
                            {this.form.email.message &&
                                <small className="text-danger">{this.form.email.message}</small>
                            }
                        </div>
                        <div className={`form-group ${this.form.password.message && 'has-error'}`}>
                            <label className="control-label">Password</label>
                            <input className="form-control" type="password" placeholder="******" value={this.form.password.value} onChange={(event) => this.handleChange(event, 'password')}/>
                            {this.form.password.message &&
                            <small className="text-danger">{this.form.password.message}</small>
                            }
                        </div>

                        <button type="submit" className="btn btn-block btn-primary">
                            <span className="glyphicon glyphicon-log-in"/> Login
                        </button>

                        <div className="hidden">{this.form.render}</div>
                    </form>
                </div>
            </div>
        );
    }
}
