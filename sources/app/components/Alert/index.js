import './styles/index.scss';

import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('alertStore')
@observer
export default class Alert extends Component {

    render() {
        const { alertStore } = this.props;

        return (
            <div className={`alert-component ${alertStore.isOpen ? 'open' : ''}`}>
                <div className={`alert ${alertStore.alertType === 'success' ? 'alert-success' : alertStore.alertType === 'warning' ? 'alert-warning' : 'alert-danger'}`} role="alert">
                    {alertStore.alertMessage}
                </div>
            </div>
        );
    }
}
