import React, { Component } from 'react';
import { observable, action } from 'mobx';
import { observer, inject } from 'mobx-react';
import moment from 'moment';

@inject('authStore', 'articlesStore', 'alertStore')
@observer
export default class ArticleCard extends Component {
    @observable isEdit = false;
    @observable activeRating = 0;


    @observable form = {
        tags: '',
        title: '',
        description: ''

    };

    constructor(props) {
        super(props);

        this.form.tags = this.props.article.tags.join(' ');
        this.form.title = this.props.article.title;
        this.form.description = this.props.article.description;

    }

    @action
    handleRating(actionType) {
        const { authStore, articlesStore, article, alertStore } = this.props;

        if (!authStore.isAuthenticated) {
            return alertStore.showWarning('Please log-in');
        }

        switch (this.activeRating) {
            case 0:
                if (actionType === 'like') {
                    this.activeRating = 1;
                    articlesStore.updateRatingCount(article, 1);
                } else {
                    this.activeRating = -1;
                    articlesStore.updateRatingCount(article, -1);
                }
                break;
            default:
                switch (this.activeRating) {
                    case 1:
                        if (actionType === 'like') {
                            this.activeRating = 0;
                            articlesStore.updateRatingCount(article, -1);
                        } else {
                            this.activeRating = -1;
                            articlesStore.updateRatingCount(article, -2);
                        }
                        break;
                    case -1:
                        if (actionType === 'like') {
                            this.activeRating = 1;
                            articlesStore.updateRatingCount(article, 2);
                        } else {
                            this.activeRating = 0;
                            articlesStore.updateRatingCount(article, 1);
                        }
                        break;
                }
        }
    }

    @action
    handleSubmit(event) {
        event.preventDefault();

        const { article, alertStore, articlesStore } = this.props;

        articlesStore.updateArticle(article._id, this.form);

        alertStore.showSuccess('Successfully updated!');

        this.isEdit = false;
    }

    @action
    handleCancel() {
        this.isEdit = false;
    }

    @action
    handleChange(event, key) {
        this.form[key] = event.target.value.trim();
    }

    @action
    handleInitEdit() {
        this.isEdit = true;
    }

    @action
    handleFilter(key, value) {
        const { articlesStore } = this.props;

        switch(key){
            case 'tag':
                if (articlesStore.chosenTag === value) {
                    articlesStore.chosenTag = null;
                } else {
                    articlesStore.chosenTag = value;
                }
                break;
            case 'user':
                if (articlesStore.chosenUser === value) {
                    articlesStore.chosenUser = null;
                } else {
                    articlesStore.chosenUser = value;
                }
                break;
        }
        articlesStore.filterBy();
    }

    render() {
        const { authStore, articlesStore, article } = this.props;

        return (
            <div className="article-card">
                <div className="thumbnail">

                    {authStore.isAuthenticated &&
                    <div className="btn-group article-actions" role="group">
                        {this.isEdit ?
                            <button type="button" className="btn btn-success" onClick={(e) => this.handleSubmit(e)}>
                                <span className="glyphicon glyphicon-floppy-saved"/>
                            </button>
                            :
                            <button type="button" className="btn btn-warning" onClick={() => this.handleInitEdit()}>
                                <span className="glyphicon glyphicon-pencil"/>
                            </button>
                        }

                        {this.isEdit ?
                            <button type="button" className="btn btn-danger" onClick={(e) => this.handleCancel(e)}>
                                <span className="glyphicon glyphicon-remove"/>
                            </button>
                            :
                            <button type="button" className="btn btn-danger" onClick={() => articlesStore.deleteArticle(article._id)}>
                                <span className="glyphicon glyphicon-remove"/>
                            </button>
                        }
                    </div>
                    }

                    <img className="img-responsive" src={article.image} alt="..."/>

                    <div className="caption">

                        <div className="tags-list">
                            {article.tags.map((tag, i) =>
                                <button type="button" key={i} className={`btn btn-sm ${tag===articlesStore.chosenTag ? 'btn-info' : 'btn-default'}`} onClick={() => this.handleFilter('tag', tag)}>{tag}</button>
                            )}
                        </div>

                        <h3>{article.title}</h3>

                        <p>{article.description}</p>

                        <p className="createdts-bar">{moment(article.createdAt).format('LLLL')}</p>

                        <div className="rating-bar">
                            <button type="button" className={`btn ${this.activeRating === 0 || this.activeRating === -1 ? 'btn-primary' : 'btn-success'}`} role="button" onClick={this.handleRating.bind(this, 'like')}>
                                <span className="glyphicon glyphicon-chevron-up" aria-hidden="true"/>
                            </button>

                            <span className={`rating-field ${article.ratingCount > 0 ? 'text-success' : article.ratingCount < 0 ? 'text-danger' : ''}`}>{article.ratingCount > 0 && '+'}{article.ratingCount}</span>

                            <button type="button" className={`btn ${this.activeRating === 0 || this.activeRating === 1 ? 'btn-primary' : 'btn-danger'}`} role="button" onClick={this.handleRating.bind(this, 'dislike')}>
                                <span className="glyphicon glyphicon-chevron-down" aria-hidden="true"/>
                            </button>
                        </div>

                        <button type="button" className={`btn ${article.author._id === articlesStore.chosenUser ? 'btn-info' : 'btn-default'} author-field`} onClick={() => this.handleFilter('user',article.author._id)}>
                            @{article.author.username}
                        </button>
                    </div>
                </div>

                {this.isEdit &&
                <div className="edit-form">
                    <form className="edit-form-groups" onSubmit={(e) => this.handleSubmit(e)}>
                        <button type="submit" className="hidden"/>

                        <div className="form-group tags-input">
                            <label className="control-label">Enter tags:</label>
                            <input className="form-control" type="text" value={this.form.tags} onChange={(e) => this.handleChange(e, 'tags')}/>
                        </div>

                        <div className="form-group title-input">
                            <label className="control-label">Enter title:</label>
                            <input className="form-control" type="text" value={this.form.title} onChange={(e) => this.handleChange(e, 'title')}/>
                        </div>

                        <div className="form-group description-input">
                            <label className="control-label">Enter description:</label>
                            <textarea type="text" className="form-control" value={this.form.description} onChange={(e) => this.handleChange(e, 'description')}/>
                        </div>

                    </form>
                </div>
                }
            </div>

        );
    }
}
