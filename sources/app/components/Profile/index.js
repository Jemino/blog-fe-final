import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action, runInAction } from 'mobx';

@inject('authStore', 'alertStore')
@observer
export default class Profile extends Component {

    @action
    async handleLogout(event) {
        event.preventDefault();
        const { authStore, alertStore } = this.props;

        const response  = await authStore.logout();

        if (response.status) {
            alertStore.showSuccess('Successfully logged-out');
        } else {
            alertStore.showDanger(`Failed to log-out: ${response.message}`);
        }
    }

    render() {
        const { authStore } = this.props;

        return (
            <div className="profile-component">
                <div className="panel panel-primary">
                    <div className="panel-heading">
                        <h3 className="panel-title">Hi, {authStore.user.username}</h3>
                    </div>
                    <div className="panel-body">
                        <pre>{JSON.stringify(authStore.user, null, 2)}</pre>
                        <button type="button" className="btn btn-block btn-danger" onClick={(e) => this.handleLogout(e)}>
                            <span className="glyphicon glyphicon-log-out"/> Logout
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}
