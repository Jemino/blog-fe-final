import './styles/index.scss';

import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import DevTools from 'mobx-react-devtools';

import NavBar from './components/NavBar';
import Profile from './components/Profile';
import AuthForm from './components/AuthForm';
import ArticleCard from './components/ArticleCard';
import ArticleCardForm from './components/ArticleCardForm';
import Alert from './components/Alert';

@inject('authStore', 'articlesStore')
@observer
export default class App extends Component {

    render() {
        const { authStore, articlesStore } = this.props;

        return (
            <div className="app-component">
                <Alert/>

                <NavBar/>

                <div className="container">
                    <div className="row">
                        <div className="col-sm-9">
                            {articlesStore.articles.map((article, i) =>
                                <ArticleCard key={i} article={article}/>
                            )}

                            {/*{authStore.isAuthenticated &&*/}
                                {/*<ArticleCardForm/>*/}
                            {/*}*/}
                        </div>
                        <div className="col-sm-3">
                            {authStore.isAuthenticated ?
                                <Profile/>
                                :
                                <AuthForm/>
                            }
                        </div>
                    </div>
                </div>

                <DevTools/>
            </div>
        );
    }
}
