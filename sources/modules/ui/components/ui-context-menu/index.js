import './styles/index.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class UIContextMenu extends Component {
    static propTypes = {
        position: PropTypes.object.isRequired,
        isOpen: PropTypes.bool.isRequired,
        onClose: PropTypes.func
    };

    static defaultProps = {
        position: {
            top: 0,
            left: 0
        },
        isOpen: true
    };

    componentDidMount() {
        const { onClose } = this.props;

        document.addEventListener('click', (event) => {
            const { contextMenu } = this.refs;

            let isContextMenu = false;

            event.path.map((element) => {
                if (element === contextMenu) {
                    isContextMenu = true;
                }
            });

            if (!isContextMenu) {
                onClose();
            }
        });
    }

    render() {
        const { position, children, isOpen } = this.props;

        if (!isOpen) {
            return null;
        }

        return (
            <div className="ui-context-menu" style={{ top: position.top, left: position.left }} ref="contextMenu">
                {children}
            </div>
        );
    }
}
