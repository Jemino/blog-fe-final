import './styles/index.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class UIUploadButton extends Component {
    static propTypes = {
        accept: PropTypes.string,
        multiple: PropTypes.bool,
        onChange: PropTypes.func.isRequired
    };

    static defaultProps = {
        accept: '*',
        multiple: false
    };

    componentDidMount() {
        const { file } = this.refs;
        const { onChange } = this.props;

        file.addEventListener('change', (event) => {
            onChange(event.target.files);
        });
    }

    render() {
        const { accept, multiple } = this.props;

        return (
            <div className="ui-upload-button">
                <input type="file" accept={accept} multiple={multiple} ref="file"/>
            </div>
        );
    }
}
