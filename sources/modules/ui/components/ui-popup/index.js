import './styles/index.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class UIPopup extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        isOpen: PropTypes.bool.isRequired,
        onClose: PropTypes.func
    };

    static defaultProps = {
        title: 'Title',
        isOpen: false
    };

    render() {
        const { title, children, isOpen, onClose } = this.props;

        if (!isOpen) {
            document.body.classList.remove('ui-popup-overflow-hidden');
            return null;
        }

        document.body.classList.add('ui-popup-overflow-hidden');

        return (
            <div className="ui-popup">
                <div className="overlay" onClick={onClose}/>

                <div className="window">
                    <h3 className="title">
                        {title}
                        <span className="close pull-right" onClick={onClose}>&times;</span>
                    </h3>

                    <hr/>

                    <div className="content">
                        {children}
                    </div>
                </div>
            </div>
        );
    }
}
