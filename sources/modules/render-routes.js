import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';

export function renderRoutes(routes) {
    return (
        <Switch>
            {routes.map((route, i) =>
                <Route key={i} path={route.path} exact={route.exact} render={(props) =>
                    route.private ?
                        <Redirect to={{
                            pathname: '/login',
                            state: { from: props.location }
                        }}/>
                        :
                        route.redirect ?
                            <Redirect to={route.redirect}/>
                            :
                            <route.component {...props} routes={route.routes || []}/>
                }/>
            )}
        </Switch>
    );
}
